Microsoft Edge Context Menu Enhancement
_______________________________________

This application adds Back, Forward, and
Refresh buttons to the context menu of
the default browser from Windows 10,
Microsoft Edge, mimicking a really useful
feature from Mozilla Firefox.

There are still some small glitches that
occur, probably because of the rather
unstable nature of the builds from the
Windows Insider Program. There are huge
differences between how Edge operates in
2 consecutive builds, which is a clear
sign that Microsoft is actively working
on the browser/system, which is a good
sign from their part.

The application is free to use, modify,
redistribute etc. The software is provided
under the MIT license, reproduced bellow
for your convinience:

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT 
WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
PURPOSE AND NONINFRINGEMENT. IN NO EVENT 
SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
IN CONNECTION WITH THE SOFTWARE OR THE USE 
OR OTHER DEALINGS IN THE SOFTWARE.

Thanks for trying it out!

