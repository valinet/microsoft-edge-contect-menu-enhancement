﻿namespace EdgeContextMenu
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.forward = new System.Windows.Forms.PictureBox();
            this.back = new System.Windows.Forms.PictureBox();
            this.refresh = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.hideTimer = new System.Windows.Forms.Timer(this.components);
            this.refreshTimer = new System.Windows.Forms.Timer(this.components);
            this.backTimer = new System.Windows.Forms.Timer(this.components);
            this.forwardTimer = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.forward)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.back)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.refresh)).BeginInit();
            this.SuspendLayout();
            // 
            // forward
            // 
            this.forward.Location = new System.Drawing.Point(44, 0);
            this.forward.Name = "forward";
            this.forward.Size = new System.Drawing.Size(44, 44);
            this.forward.TabIndex = 9;
            this.forward.TabStop = false;
            this.toolTip1.SetToolTip(this.forward, "Forward (Shift + Backspace)");
            this.forward.MouseDown += new System.Windows.Forms.MouseEventHandler(this.forward_MouseDown);
            this.forward.MouseEnter += new System.EventHandler(this.forward_MouseEnter);
            this.forward.MouseLeave += new System.EventHandler(this.forward_MouseLeave);
            this.forward.MouseUp += new System.Windows.Forms.MouseEventHandler(this.forward_MouseUp);
            // 
            // back
            // 
            this.back.Location = new System.Drawing.Point(0, 0);
            this.back.Name = "back";
            this.back.Size = new System.Drawing.Size(44, 44);
            this.back.TabIndex = 8;
            this.back.TabStop = false;
            this.toolTip1.SetToolTip(this.back, "Back (Backspace)");
            this.back.MouseDown += new System.Windows.Forms.MouseEventHandler(this.back_MouseDown);
            this.back.MouseEnter += new System.EventHandler(this.back_MouseEnter);
            this.back.MouseLeave += new System.EventHandler(this.back_MouseLeave);
            this.back.MouseUp += new System.Windows.Forms.MouseEventHandler(this.back_MouseUp);
            // 
            // refresh
            // 
            this.refresh.Location = new System.Drawing.Point(88, 0);
            this.refresh.Name = "refresh";
            this.refresh.Size = new System.Drawing.Size(44, 44);
            this.refresh.TabIndex = 10;
            this.refresh.TabStop = false;
            this.toolTip1.SetToolTip(this.refresh, "Refresh (F5)");
            this.refresh.MouseDown += new System.Windows.Forms.MouseEventHandler(this.refresh_MouseDown);
            this.refresh.MouseEnter += new System.EventHandler(this.refresh_MouseEnter);
            this.refresh.MouseLeave += new System.EventHandler(this.refresh_MouseLeave);
            this.refresh.MouseUp += new System.Windows.Forms.MouseEventHandler(this.refresh_MouseUp);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // hideTimer
            // 
            this.hideTimer.Interval = 10;
            this.hideTimer.Tick += new System.EventHandler(this.hideTimer_Tick);
            // 
            // refreshTimer
            // 
            this.refreshTimer.Interval = 1;
            this.refreshTimer.Tick += new System.EventHandler(this.refreshTimer_Tick);
            // 
            // backTimer
            // 
            this.backTimer.Interval = 1;
            this.backTimer.Tick += new System.EventHandler(this.backTimer_Tick);
            // 
            // forwardTimer
            // 
            this.forwardTimer.Interval = 1;
            this.forwardTimer.Tick += new System.EventHandler(this.forwardTimer_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(0, 0);
            this.Controls.Add(this.refresh);
            this.Controls.Add(this.forward);
            this.Controls.Add(this.back);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form1";
            this.Opacity = 0D;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Form1";
            this.TransparencyKey = System.Drawing.SystemColors.Control;
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
            ((System.ComponentModel.ISupportInitialize)(this.forward)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.back)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.refresh)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.PictureBox forward;
        private System.Windows.Forms.PictureBox back;
        private System.Windows.Forms.PictureBox refresh;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Timer hideTimer;
        private System.Windows.Forms.Timer refreshTimer;
        private System.Windows.Forms.Timer backTimer;
        private System.Windows.Forms.Timer forwardTimer;
    }
}

