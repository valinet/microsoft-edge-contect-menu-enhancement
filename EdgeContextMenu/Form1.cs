﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsInput;
using WindowScrape;
using System.Windows.Automation;

namespace EdgeContextMenu
{
    public partial class Form1 : Form
    {
        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();
        [DllImport("user32.dll", SetLastError = true)]
        static extern uint GetWindowThreadProcessId(IntPtr hWnd, out uint lpdwProcessId);
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern int GetWindowText(IntPtr hWnd, StringBuilder lpString, int nMaxCount);
        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        static extern int GetWindowTextLength(IntPtr hWnd);

        [DllImport("user32.dll", CharSet = CharSet.Auto,
        CallingConvention = CallingConvention.StdCall)]
        public static extern int SetWindowsHookEx(int idHook, HookProc lpfn,
        IntPtr hInstance, int threadId);
        [DllImport("user32.dll", CharSet = CharSet.Auto,
        CallingConvention = CallingConvention.StdCall)]
        public static extern bool UnhookWindowsHookEx(int idHook);
        [DllImport("user32.dll", CharSet = CharSet.Auto,
        CallingConvention = CallingConvention.StdCall)]
        public static extern int CallNextHookEx(int idHook, int nCode,
        IntPtr wParam, IntPtr lParam);
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr GetModuleHandle(string lpModuleName);

        static int hHook = 0;
        public delegate int HookProc(int nCode, IntPtr wParam, IntPtr lParam);
        HookProc MouseHookProcedure;

        [StructLayout(LayoutKind.Sequential)]
        public class POINT
        {
            public int x;
            public int y;
        }

        [StructLayout(LayoutKind.Sequential)]
        public class MouseHookStruct
        {
            public POINT pt;
            public int hwnd;
            public int wHitTestCode;
            public int dwExtraInfo;
        }

        private enum MouseMessages
        {
            WM_LBUTTONDOWN = 0x0201,
            WM_LBUTTONUP = 0x0202,
            WM_MOUSEMOVE = 0x0200,
            WM_MOUSEWHEEL = 0x020A,
            WM_RBUTTONDOWN = 0x0204,
            WM_RBUTTONUP = 0x0205
        }

        private const int WH_MOUSE_LL = 14;

        [DllImport("user32.dll")]
        public static extern bool GetWindowRect(IntPtr hwnd, ref Rect rectangle);

        public struct Rect
        {
            public int Left { get; set; }
            public int Top { get; set; }
            public int Right { get; set; }
            public int Bottom { get; set; }
        }

        [DllImport("psapi.dll")]
        public static extern bool EmptyWorkingSet(IntPtr hProcess);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool SetForegroundWindow(IntPtr hWnd);

        [DllImport("user32.dll", SetLastError = true)]
        internal static extern bool MoveWindow(IntPtr hWnd, int X, int Y, int nWidth, int nHeight, bool bRepaint);

        [DllImport("User32.dll")]
        public static extern Int64 SendMessage(IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam);

        [DllImport("user32.dll")]
        static extern IntPtr GetTopWindow(IntPtr hWnd);

        [DllImport("user32.dll", SetLastError = true)]
        private static extern IntPtr GetWindow(IntPtr hWnd, uint uCmd);

        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern void mouse_event(uint dwFlags, uint dx, uint dy, uint cButtons, uint dwExtraInfo);

        private const int MOUSEEVENTF_LEFTDOWN = 0x02;
        private const int MOUSEEVENTF_LEFTUP = 0x04;
        private const int MOUSEEVENTF_RIGHTDOWN = 0x08;
        private const int MOUSEEVENTF_RIGHTUP = 0x10;

        [DllImport("user32.dll")]
        static extern bool UpdateWindow(IntPtr hWnd);

        public Form1()
        {
            InitializeComponent();
        }

        public static string GetText(IntPtr hWnd)
        {
            // Allocate correct string length first
            int length = GetWindowTextLength(hWnd);
            StringBuilder sb = new StringBuilder(length + 1);
            GetWindowText(hWnd, sb, sb.Capacity);
            return sb.ToString();
        }
        static Form1 frm;
        private void hideTimer_Tick(object sender, EventArgs e)
        {
            this.Hide();
            IntPtr fgnd = IntPtr.Zero;
            if (edge_window != IntPtr.Zero)
            {
                var x = WindowScrape.Types.HwndObject.GetWindowByhWnd(edge_window);
                foreach (var t in x.GetChildren())
                {
                    if (t.Title.Contains("Microsoft Edge")) fgnd = t.Hwnd;
                    t.Size = new Size(t.Size.Width - 1, t.Size.Height - 1);
                    t.Size = new Size(t.Size.Width + 1, t.Size.Height + 1);
                }
                x.Size = new Size(x.Size.Width - 1, x.Size.Height - 1);
                x.Size = new Size(x.Size.Width + 1, x.Size.Height + 1);
                SetForegroundWindow(fgnd);
            }
            Process.Start(Application.ExecutablePath);
            bool ret = UnhookWindowsHookEx(hHook);
            Environment.Exit(0);
            hideTimer.Enabled = false;
        }
        IntPtr edge_window = IntPtr.Zero;
        bool justStarted = true;
        public static int MouseHookProc(int nCode, IntPtr wParam, IntPtr lParam)
        {
            MouseHookStruct MyMouseHookStruct = (MouseHookStruct)
                Marshal.PtrToStructure(lParam, typeof(MouseHookStruct));

            if (nCode >= 0 && MouseMessages.WM_LBUTTONUP == (MouseMessages)wParam && !frm.isIn)
            {
                if (frm.Visible) frm.hideTimer.Enabled = true;
                else
                {
                    try
                    {
                        uint id;
                        IntPtr edge_window = GetForegroundWindow();
                        GetWindowThreadProcessId(edge_window, out id);
                        string current = Process.GetProcessById((int)id).MainModule.FileName;
                        if (current.Contains("\\ApplicationFrameHost.exe") && GetText(edge_window).Contains("Microsoft Edge"))
                        {
                            frm.edge_window = edge_window;
                            if (GetForegroundWindow() == frm.edge_window)
                            {
                                int xcoord = MyMouseHookStruct.pt.x;
                                int ycoord = MyMouseHookStruct.pt.y;
                                var w = WindowScrape.Types.HwndObject.GetWindowByhWnd(frm.edge_window);
                                if ((xcoord >= w.Location.X && xcoord <= w.Location.X + w.Size.Width && ycoord >= w.Location.Y && ycoord <= w.Location.Y + 32) || frm.justStarted)
                                {
                                    var x = WindowScrape.Types.HwndObject.GetWindowByhWnd(frm.edge_window);
                                    foreach (var t in x.GetChildren())
                                    {
                                        t.Size = new Size(t.Size.Width - 1, t.Size.Height - 1);
                                        t.Size = new Size(t.Size.Width + 1, t.Size.Height + 1);
                                    }
                                    x.Size = new Size(x.Size.Width - 1, x.Size.Height - 1);
                                    x.Size = new Size(x.Size.Width + 1, x.Size.Height + 1);
                                    frm.justStarted = false;
                                }
                            }
                        }
                    }
                    catch { }
                }
            }
                //if (GetForegroundWindow() != frm.Handle || frm.isIn == false)
               // {
                    /*Process.Start(Application.ExecutablePath);
                    Environment.Exit(0);*/
                   /* frm.Hide();
                    SendMessage(GetForegroundWindow(), 0x000F, IntPtr.Zero, IntPtr.Zero);*/
              //  }
            //}

            if (nCode >= 0 && MouseMessages.WM_RBUTTONUP == (MouseMessages)wParam)
            {
                try
                {
                    int xcoord = MyMouseHookStruct.pt.x;
                    int ycoord = MyMouseHookStruct.pt.y;
                    uint id;
                    IntPtr edge_window = GetForegroundWindow();
                    GetWindowThreadProcessId(edge_window, out id);
                    string current = Process.GetProcessById((int)id).MainModule.FileName;
                    if (current.Contains("\\ApplicationFrameHost.exe") && GetText(edge_window).Contains("Microsoft Edge"))
                    {
                        frm.edge_window = edge_window;

                        var w = WindowScrape.Types.HwndObject.GetWindowByhWnd(frm.edge_window);
                        if (!(xcoord >= w.Location.X && xcoord <= w.Location.X + 200 && ycoord >= w.Location.Y + 32 && ycoord <= w.Location.Y + 76))
                        {
                            Rect r = new Rect();
                            GetWindowRect(frm.edge_window, ref r);
                            Bitmap b = capture_class.CaptureScreentoClipboard(r.Left + 8, r.Top + 8, 1, 1);
                            Color c = b.GetPixel(0, 0);
                            if ((c.R == 36 && c.G == 36 && c.B == 36) || (c.R == 56 && c.G == 56 && c.B == 56))
                            {
                                frm.mode = true;
                            }
                            else frm.mode = false;

                            bool backEnabled = true;
                            bool forwardEnabled = true;

                            var children = AutomationElement.RootElement.FindAll(TreeScope.Children, Condition.TrueCondition);

                            foreach (AutomationElement child in children)
                            {
                                if (child.Current.ClassName.Equals("ApplicationFrameWindow"))
                                {
                                    var deeper = child.FindAll(TreeScope.Children, Condition.TrueCondition);
                                    foreach (AutomationElement d in deeper)
                                    {
                                        if (d.Current.Name.Equals("Microsoft Edge"))
                                        {
                                            var deeper2 = d.FindAll(TreeScope.Children, Condition.TrueCondition);
                                            foreach (AutomationElement d2 in deeper2)
                                            {
                                                //Console.WriteLine(d2.Current.AutomationId.ToString());
                                                if (d2.Current.AutomationId.Equals("BackButton")) //ForwardButton
                                                {
                                                    var value = d2.GetCurrentPropertyValue(AutomationElement.IsEnabledProperty);
                                                    if (value.ToString() == "False") backEnabled = false;
                                                }
                                                if (d2.Current.AutomationId.Equals("ForwardButton")) //ForwardButton
                                                {
                                                    var value = d2.GetCurrentPropertyValue(AutomationElement.IsEnabledProperty);
                                                    if (value.ToString() == "False") forwardEnabled = false;
                                                }
                                            }
                                        }

                                    }
                                }
                            }
                            if (frm.mode == false)
                            {
                                if (backEnabled)
                                {
                                    frm.back.BackgroundImage = EdgeContextMenu.Properties.Resources.back_white;
                                    frm.back.Tag = "";
                                }
                                else
                                {
                                    frm.back.BackgroundImage = EdgeContextMenu.Properties.Resources.back_white_disabled;
                                    frm.back.Tag = "disabled";
                                }
                                if (forwardEnabled)
                                {
                                    frm.forward.BackgroundImage = EdgeContextMenu.Properties.Resources.forward_white;
                                    frm.forward.Tag = "";
                                }
                                else
                                {
                                    frm.forward.BackgroundImage = EdgeContextMenu.Properties.Resources.forward_white_disabled;
                                    frm.forward.Tag = "disabled";
                                }
                                frm.refresh.BackgroundImage = EdgeContextMenu.Properties.Resources.refresh_white;
                            }
                            else
                            {
                                if (backEnabled)
                                {
                                    frm.back.BackgroundImage = EdgeContextMenu.Properties.Resources.back;
                                    frm.back.Tag = "";
                                }
                                else
                                {
                                    frm.back.BackgroundImage = EdgeContextMenu.Properties.Resources.back_disabled;
                                    frm.back.Tag = "disabled";
                                }
                                if (forwardEnabled)
                                {
                                    frm.forward.BackgroundImage = EdgeContextMenu.Properties.Resources.forward;
                                    frm.forward.Tag = "";
                                }
                                else
                                {
                                    frm.forward.BackgroundImage = EdgeContextMenu.Properties.Resources.forward_disabled;
                                    frm.forward.Tag = "disabled";
                                }
                                frm.refresh.BackgroundImage = EdgeContextMenu.Properties.Resources.refresh;
                            }
                            if (frm.Visible)
                                frm.Hide();
                            else
                            {
                                frm.Size = new Size(132, 44);
                                frm.Show();
                                frm.WindowState = FormWindowState.Normal;
                                //frm.TopMost = true;
                                frm.screens = Screen.AllScreens;
                                frm.noofscreens = frm.screens.Length;
                                int maxwidth = 0, maxheight = 0;
                                for (int i = 0; i < frm.noofscreens; i++)
                                {
                                    if (maxwidth < (frm.screens[i].Bounds.X + frm.screens[i].Bounds.Width)) maxwidth = frm.screens[i].Bounds.X + frm.screens[i].Bounds.Width;
                                    if (maxheight < (frm.screens[i].Bounds.Y + frm.screens[i].Bounds.Height)) maxheight = frm.screens[i].Bounds.Y + frm.screens[i].Bounds.Height;
                                }
                                if (xcoord + frm.Width > maxwidth) frm.Location = new Point(xcoord - frm.Width, ycoord - 43);
                                else frm.Location = new Point(xcoord, ycoord - 43);
                                if (ycoord - frm.Height < 0) frm.Location = new Point(frm.Location.X - frm.Width, frm.Location.Y + frm.Height);
                                if (ycoord >= maxheight - 100) frm.Location = new Point(frm.Location.X, ycoord - 2);
                                frm.Opacity = 1;
                                SetForegroundWindow(frm.Handle);
                            }
                        }
                    }
                }
                catch { }
            }

            GC.Collect();
            Process pProcess = Process.GetCurrentProcess();
            bool bRes = EmptyWorkingSet(pProcess.Handle);

            return CallNextHookEx(hHook, nCode, wParam, lParam);
        }

        bool mode = false;
        int noofscreens = 0;
        Screen[] screens;
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            frm = this;

            if (hHook == 0)
            {
                using (Process curProcess = Process.GetCurrentProcess())
                using (ProcessModule curModule = curProcess.MainModule)
                {
                    MouseHookProcedure = new HookProc(Form1.MouseHookProc);
                    hHook = SetWindowsHookEx(WH_MOUSE_LL,
                                     MouseHookProcedure,
                                     GetModuleHandle(curModule.ModuleName), 0);
                }
            }

            if (Environment.GetCommandLineArgs().Length > 1)
            {
                try
                {
                    int t = Convert.ToInt32(Environment.GetCommandLineArgs()[1]);
                    if (t == 1) mode = true;
                    if (t == 2)
                    {
                        try
                        {
                            // Creates an XML file from which a scheduled task is created and saved on the system. Based on an example found on StackOverflow, http://stackoverflow.com/questions/5427673/how-to-run-a-program-automatically-as-admin-on-windows-startup
                            string username = Environment.UserName;
                            string computername = Environment.MachineName;
                            string part1 = EdgeContextMenu.Properties.Resources.part1;
                            string part2 = EdgeContextMenu.Properties.Resources.part2;
                            string part3 = EdgeContextMenu.Properties.Resources.part3;
                            string part4 = EdgeContextMenu.Properties.Resources.part4;
                            string final = part1 + username + part2 + computername + "\\" + username + part3 + Application.ExecutablePath + part4;
                            System.IO.StreamWriter sw = new System.IO.StreamWriter(Application.StartupPath + "\\apply.xml");
                            sw.Write(final);
                            sw.Close();
                            Process pr = new Process();
                            ProcessStartInfo pi = new ProcessStartInfo();
                            pi.FileName = "cmd.exe";
                            pi.Arguments = "/c schtasks /create /tn \"Start Microsoft Edge Context Menu Enhancement elevated\" /xml \"" + Application.StartupPath + "\\apply.xml\"";
                            pr.StartInfo = pi;
                            pr.Start();
                            pr.WaitForExit();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Failed to register for running at startup elevated.\r\n\r\n" + ex.Message, "ThinkPad LEDs Control", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
                catch { }
            }

            if (mode == false)
            {
                back.BackgroundImage = EdgeContextMenu.Properties.Resources.back_white;
                forward.BackgroundImage = EdgeContextMenu.Properties.Resources.forward_white;
                refresh.BackgroundImage = EdgeContextMenu.Properties.Resources.refresh_white;
            }
            else
            {
                back.BackgroundImage = EdgeContextMenu.Properties.Resources.back;
                forward.BackgroundImage = EdgeContextMenu.Properties.Resources.forward;
                refresh.BackgroundImage = EdgeContextMenu.Properties.Resources.refresh;
            }

            GC.Collect();
            Process pProcess = Process.GetCurrentProcess();
            bool bRes = EmptyWorkingSet(pProcess.Handle);

            this.Hide();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            bool ret = UnhookWindowsHookEx(hHook);
            base.OnClosing(e);
        }

        bool isIn = false;
        private void back_MouseEnter(object sender, EventArgs e)
        {
            if (back.Tag.ToString() != "disabled")
            {
                if (mode == true) back.BackgroundImage = EdgeContextMenu.Properties.Resources.back_hover;
                else back.BackgroundImage = EdgeContextMenu.Properties.Resources.back_white_hover;
            }
            isIn = true;
        }

        private void forward_MouseEnter(object sender, EventArgs e)
        {
            if (forward.Tag.ToString() != "disabled")
            {
                if (mode == true) forward.BackgroundImage = EdgeContextMenu.Properties.Resources.forward_hover;
                else forward.BackgroundImage = EdgeContextMenu.Properties.Resources.forward_white_hover;
            }
            isIn = true;
        }

        private void refresh_MouseEnter(object sender, EventArgs e)
        {
            if (mode == true) refresh.BackgroundImage = EdgeContextMenu.Properties.Resources.refresh_hover;
            else refresh.BackgroundImage = EdgeContextMenu.Properties.Resources.refresh_white_hover;
            isIn = true;
        }

        private void back_MouseLeave(object sender, EventArgs e)
        {
            if (back.Tag.ToString() != "disabled")
            {
                if (mode == true) back.BackgroundImage = EdgeContextMenu.Properties.Resources.back;
                else back.BackgroundImage = EdgeContextMenu.Properties.Resources.back_white;
            }
            isIn = false;
        }

        private void forward_MouseLeave(object sender, EventArgs e)
        {
            if (forward.Tag.ToString() != "disabled")
            {
                if (mode == true) forward.BackgroundImage = EdgeContextMenu.Properties.Resources.forward;
                else forward.BackgroundImage = EdgeContextMenu.Properties.Resources.forward_white;
            }
            isIn = false;
        }

        private void refresh_MouseLeave(object sender, EventArgs e)
        {
            if (mode == true) refresh.BackgroundImage = EdgeContextMenu.Properties.Resources.refresh;
            else refresh.BackgroundImage = EdgeContextMenu.Properties.Resources.refresh_white;
            isIn = false;
        }

        private void back_MouseDown(object sender, MouseEventArgs e)
        {
            if (back.Tag.ToString() != "disabled")
            {
                if (mode == true) back.BackgroundImage = EdgeContextMenu.Properties.Resources.back_press;
                else back.BackgroundImage = EdgeContextMenu.Properties.Resources.back_white_press;
            }
        }

        private void forward_MouseDown(object sender, MouseEventArgs e)
        {
            if (forward.Tag.ToString() != "disabled")
            {
                if (mode == true) forward.BackgroundImage = EdgeContextMenu.Properties.Resources.forward_press;
                else forward.BackgroundImage = EdgeContextMenu.Properties.Resources.forward_white_press;
            }
        }

        private void refresh_MouseDown(object sender, MouseEventArgs e)
        {
            if (mode == true) refresh.BackgroundImage = EdgeContextMenu.Properties.Resources.refresh_press;
            else refresh.BackgroundImage = EdgeContextMenu.Properties.Resources.refresh_white_press;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.Hide();
            timer1.Enabled = false;
        }
        bool backRClick = false;
        bool forwardRClick = false;
        private void back_MouseUp(object sender, MouseEventArgs e)
        {
            this.Hide();

            if (e.Button == MouseButtons.Left) backRClick = false;
            else if (e.Button == MouseButtons.Right) backRClick = true;

            backTimer.Enabled = true;
        }

        private void forward_MouseUp(object sender, MouseEventArgs e)
        {
            this.Hide();

            if (e.Button == MouseButtons.Left) forwardRClick = false;
            else if (e.Button == MouseButtons.Right) forwardRClick = true;

            forwardTimer.Enabled = true;
        }

        private void refresh_MouseUp(object sender, MouseEventArgs e)
        {
            this.Hide();

            refreshTimer.Enabled = true;
        }
        void wrapup(string button, bool right_click)
        {
            var children = AutomationElement.RootElement.FindAll(TreeScope.Children, Condition.TrueCondition);

            foreach (AutomationElement child in children)
            {
                if (child.Current.ClassName.Equals("ApplicationFrameWindow"))
                {
                    var deeper = child.FindAll(TreeScope.Children, Condition.TrueCondition);
                    foreach (AutomationElement d in deeper)
                    {
                        if (d.Current.Name.Equals("Microsoft Edge"))
                        {
                            var deeper2 = d.FindAll(TreeScope.Children, Condition.TrueCondition);
                            foreach (AutomationElement d2 in deeper2)
                            {
                                Console.WriteLine(d2.Current.AutomationId.ToString());
                                if (d2.Current.AutomationId.Equals(button)) //ForwardButton // StopRefreshButton
                                {
                                    Point cur = Cursor.Position;
                                    System.Windows.Point p = d2.GetClickablePoint();
                                    Cursor.Position = new Point((int)p.X, (int)p.Y);
                                    if (right_click)
                                    {
                                        mouse_event(MOUSEEVENTF_RIGHTDOWN | MOUSEEVENTF_RIGHTUP, (uint)p.X, (uint)p.Y, 0, 0);
                                        if (edge_window != IntPtr.Zero)
                                        {
                                            var x = WindowScrape.Types.HwndObject.GetWindowByhWnd(edge_window);
                                            foreach (var t in x.GetChildren())
                                            {
                                                t.Size = new Size(t.Size.Width - 1, t.Size.Height - 1);
                                                t.Size = new Size(t.Size.Width + 1, t.Size.Height + 1);
                                            }
                                            x.Size = new Size(x.Size.Width - 1, x.Size.Height - 1);
                                            x.Size = new Size(x.Size.Width + 1, x.Size.Height + 1);
                                        }
                                        Process.Start(Application.ExecutablePath);
                                        bool reto = UnhookWindowsHookEx(hHook);
                                        Environment.Exit(0);
                                    }
                                    else mouse_event(MOUSEEVENTF_LEFTDOWN | MOUSEEVENTF_LEFTUP, (uint)p.X, (uint)p.Y, 0, 0);
                                    Cursor.Position = cur;
                                }
                            }
                        }

                    }
                }
            }

            //I.Keyboard.KeyPress(WindowsInput.Native.VirtualKeyCode.BROWSER_REFRESH);
            IntPtr fgnd = IntPtr.Zero;
            if (edge_window != IntPtr.Zero)
            {
                var x = WindowScrape.Types.HwndObject.GetWindowByhWnd(edge_window);
                foreach (var t in x.GetChildren())
                {
                    if (t.Title.Contains("Microsoft Edge")) fgnd = t.Hwnd;
                    t.Size = new Size(t.Size.Width - 1, t.Size.Height - 1);
                    t.Size = new Size(t.Size.Width + 1, t.Size.Height + 1);
                }
                x.Size = new Size(x.Size.Width - 1, x.Size.Height - 1);
                x.Size = new Size(x.Size.Width + 1, x.Size.Height + 1);
                SetForegroundWindow(fgnd);
            }
            Process.Start(Application.ExecutablePath);
            bool ret = UnhookWindowsHookEx(hHook);
            Environment.Exit(0);
        }
        private void refreshTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                wrapup("StopRefreshButton", false);
            }
            catch { }
        }

        private void backTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                wrapup("BackButton", backRClick);
            }
            catch { }
        }

        private void forwardTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                wrapup("ForwardButton", forwardRClick);
            }
            catch { }
        }
    }
}
